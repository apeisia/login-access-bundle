<?php

namespace Apeisia\LoginAccess\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Apeisia\LoginAccess\Entity\AbstractAccount;
use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Entity\ACLEntry;
use Apeisia\LoginAccess\Entity\Invitation;
use Apeisia\LoginAccess\Entity\LoginAccess;
use Apeisia\LoginAccess\Service\RelationConfiguration;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;
use ReflectionClass;
use ReflectionException;

#[AsDoctrineListener(event: Events::loadClassMetadata, priority: 0, connection: 'default')]
class DoctrineMetadataSubscriber implements EventSubscriber
{
    public function __construct(private readonly RelationConfiguration $relationConfiguration)
    {
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     * @throws ReflectionException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        /** @var ClassMetadata $metaData */
        $metaData = $eventArgs->getClassMetadata();
        $class    = $metaData->getReflectionClass();

        if (!$class) {
            $class = new ReflectionClass($metaData->getName());
        }

        if ($class->isSubclassOf(AbstractLogin::class) && !$metaData->hasAssociation('accesses')) {
            $metaData->mapOneToMany([
                'fieldName'    => 'accesses',
                'targetEntity' => $this->relationConfiguration->loginAccessClass,
                'mappedBy'     => 'login',
                'cascade'      => ['persist', 'remove'],
            ]);
        }
        if ($class->isSubclassOf(LoginAccess::class) && !$metaData->hasAssociation('login')) {
            $metaData->mapManyToOne([
                'fieldName'    => 'login',
                'targetEntity' => $this->relationConfiguration->loginClass,
                'inversedBy'   => 'accesses',
                'cascade'      => ['persist'],
                'joinColumns'  => [
                    [
                        'name'                 => 'login_id',
                        'referencedColumnName' => 'id',
                        'nullable'             => false,
                        'onDelete'             => 'CASCADE',
                    ],
                ],
            ]);
            $metaData->mapManyToOne([
                'fieldName'    => 'account',
                'targetEntity' => $this->relationConfiguration->accountClass,
                'inversedBy'   => 'accesses',
                'cascade'      => ['persist'],
                'joinColumns'  => [
                    [
                        'name'                 => 'account_id',
                        'referencedColumnName' => 'id',
                        'nullable'             => false,
                        'onDelete'             => 'CASCADE',
                    ],
                ],
            ]);
            $metaData->mapOneToMany([
                'fieldName'    => 'aclEntries',
                'targetEntity' => ACLEntry::class,
                'mappedBy'     => 'loginAccess',
                'cascade'      => ['persist'],
            ]);
        }
        if ($class->isSubclassOf(AbstractAccount::class) && !$metaData->hasAssociation('accesses')) {
            $metaData->mapOneToMany([
                'fieldName'    => 'accesses',
                'targetEntity' => $this->relationConfiguration->loginAccessClass,
                'mappedBy'     => 'account',
                'cascade'      => ['persist', 'remove'],
            ]);
            if ($this->relationConfiguration->enableInvitations) {
                $metaData->mapOneToMany([
                    'fieldName'    => 'pendingInvitations',
                    'targetEntity' => $this->relationConfiguration->invitationClass,
                    'mappedBy'     => 'account',
                    'cascade'      => ['persist', 'remove'],
                ]);
            }
        }
        if ($this->relationConfiguration->enableInvitations && $class->isSubclassOf(Invitation::class) && !$metaData->hasAssociation('account')) {
            $metaData->mapManyToOne([
                'fieldName'    => 'account',
                'targetEntity' => $this->relationConfiguration->accountClass,
                'inversedBy'   => 'pendingInvitations',
            ]);
        }
        if ($class->getName() === ACLEntry::class && !$metaData->hasAssociation('loginAccess')) {
            $metaData->mapManyToOne([
                'fieldName'    => 'loginAccess',
                'targetEntity' => $this->relationConfiguration->loginAccessClass,
                'inversedBy'   => 'aclEntries',
            ]);
        }
    }
}
