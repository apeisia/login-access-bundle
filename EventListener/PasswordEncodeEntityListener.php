<?php

namespace Apeisia\LoginAccess\EventListener;

use Apeisia\LoginAccess\Entity\AbstractLogin;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class PasswordEncodeEntityListener
{
    public function __construct(private readonly UserPasswordHasherInterface $userPasswordHasher)
    {
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $obj = $args->getObject();

        if ($obj instanceof AbstractLogin) {
            $this->hashPassword($obj);
        }
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $obj = $args->getObject();

        if ($obj instanceof AbstractLogin && $obj->getPlainPassword() !== null) {
            $this->hashPassword($obj);


            $em   = $args->getEntityManager();
            $uow  = $em->getUnitOfWork();
            $meta = $em->getClassMetadata(get_class($obj));
            $uow->recomputeSingleEntityChangeSet($meta, $obj);

        }
    }

    private function hashPassword(AbstractLogin $login): void
    {
        $login->setPassword($this->userPasswordHasher->hashPassword($login, $login->getPlainPassword()));
    }

}
