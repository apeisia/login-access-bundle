<?php

namespace Apeisia\LoginAccess\EventListener;

use Apeisia\LoginAccess\Login\AccountAccessDeniedException;
use Apeisia\LoginAccess\Login\AccountSelectionRequiredException;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class KernelResponseListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return array(
            KernelEvents::EXCEPTION => [
                'onKernelException',
                129
            ],
        );
    }

    public function __construct(private readonly SerializerInterface $serializer)
    {
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        if (!$event->isMainRequest()) return;

        $e = $event->getThrowable();
        if ($e instanceof AccountSelectionRequiredException) {

            $accounts = $e->getAccounts();
            $ctx      = SerializationContext::create();
            $ctx->setGroups(['select']);

            // workaround for the moment:
            // our current session gets destroyed if we just continue here.

            header('Status: 495 Account selection required');
            header('Content-Type: text/json');
            if (array_key_exists('HTTP_ORIGIN', $_SERVER)) {
                header('Access-Control-Allow-Credentials: true');
                header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                header('Access-Control-Expose-Headers: x-debug-token, x-debug-token-link, x-frontend-rev');
            }
            echo $this->serializer->serialize($accounts, 'json', $ctx);
            exit;

//            $r = new Response($this->serializer->serialize($accounts, 'json', $ctx));
//            $r->setStatusCode(495, 'Account selection required');
//
//
//            $event->setResponse($r);
//            $event->stopPropagation();
        }
        if ($e instanceof AccountAccessDeniedException) {
            $event->setResponse(
                new Response(
                    $this->serializer->serialize([
                        'error' => $e->getMessage(),
                    ], 'json'),
                    401
                )
            );
            $event->stopPropagation();
        }
    }
}
