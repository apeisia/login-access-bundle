<?php

namespace Apeisia\LoginAccess\Login;

use Apeisia\LoginAccess\Entity\AbstractAccount;
use Exception;

class AccountSelectionRequiredException extends Exception
{
    /**
     * @var AbstractAccount[]
     */
    private array $accounts;

    public function __construct(array $accounts)
    {
        parent::__construct();
        $this->accounts = $accounts;
    }

    public function getAccounts(): array
    {
        return $this->accounts;
    }
}
