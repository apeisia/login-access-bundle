<?php

namespace Apeisia\LoginAccess\Login;

use Apeisia\LoginAccess\Entity\AbstractAccount;
use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Entity\LoginAccess;
use Apeisia\LoginAccess\Service\AccountSelectionService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class AccountManager
{
    /**
     * @var AbstractAccount[]
     */
    private array $accounts = [];
    /**
     * @var LoginAccount[]
     */
    private array $loginAccounts = [];

    public function __construct(
        private readonly EntityManagerInterface  $em,
        private readonly AccountSelectionService $accountSelectionService,
        private readonly string                  $accountClass,
        private readonly string                  $loginAccessClass
    )
    {
    }

    /**
     * @param AbstractAccount|null $account
     * @return LoginAccess|null
     * @throws Exception
     */
    public function getLoginAccess(AbstractAccount $account = null): ?LoginAccess
    {
        return $this->getLoginAccountOrException($account, 'LoginAccess')->getLoginAccess();
    }

    /**
     * @param AbstractLogin $login
     * @param null $accountId
     * @param null $originalAccountId
     * @return AbstractAccount|null
     * @throws AccountSelectionRequiredException
     */
    public function getAccount(AbstractLogin $login, $accountId = null, $originalAccountId = null): ?AbstractAccount
    {
        $cacheKey = $login->getId() . ':' . $accountId . ':' . $originalAccountId;
        if (!array_key_exists($cacheKey, $this->accounts)) {
            $la                                 = new LoginAccount(
                $this->em,
                $this->accountSelectionService,
                $login,
                $accountId,
                $originalAccountId,
                $this->accountClass,
                $this->loginAccessClass
            );
            $acc                                = $la->getAccount();
            $this->accounts[$cacheKey]          = $acc;
            $this->loginAccounts[$acc->getId()] = $la;
        }

        return $this->accounts[$cacheKey];
    }

    /**
     * @param AbstractAccount $account
     * @return bool
     * @throws Exception
     */
    public function isSwitchedAccount(AbstractAccount $account): bool
    {
        return count($this->getSwitchAccountHierarchy($account)) > 0;
    }

    /**
     * @param AbstractAccount $account
     * @return AbstractAccount[]
     * @throws Exception
     */
    public function getSwitchAccountHierarchy(AbstractAccount $account): array
    {
        return $this->getLoginAccountOrException($account, 'SwitchAccountHierarchy')->getSwitchAccountHierarchy();
    }

    /**
     * @param AbstractAccount $account
     * @return AbstractAccount|null
     * @throws Exception
     */
    public function getSwitchFrom(AbstractAccount $account): ?AbstractAccount
    {
        $acc = $this->getLoginAccountOrException($account, 'SwitchFrom')->getOriginalAccess();

        return $acc?->getAccount();
    }

    /**
     * Get an array of all accounts that both given logins have access to.
     *
     * @param AbstractLogin $login1
     * @param AbstractLogin $login2
     *
     * @return AbstractAccount[]
     */
    public function getCommonAccounts(AbstractLogin $login1, AbstractLogin $login2): array
    {
        $commonAccounts = [];
        $accountsLogin1 = [];

        foreach ($login1->getAccesses() as $access) {
            $accountsLogin1[] = $access->getAccount();
        }

        foreach ($login2->getAccesses() as $access) {
            $account = $access->getAccount();

            if (array_search($account, $accountsLogin1, true)) {
                $commonAccounts[] = $account;
            }
        }

        return $commonAccounts;
    }

    /**
     * @param AbstractAccount $account
     * @param $messageKey
     * @return LoginAccount
     * @throws Exception
     */
    private function getLoginAccountOrException(AbstractAccount $account, $messageKey): LoginAccount
    {
        $id = $account->getId();
        if (!array_key_exists($id, $this->loginAccounts)) {
            throw new Exception('asking for a ' . $messageKey . ' for an account that is not loaded via getAccount()');
        }

        return $this->loginAccounts[$id];
    }

}
