<?php

namespace Apeisia\LoginAccess\Login;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class AccountAccessDeniedException extends AccessDeniedHttpException
{

}
