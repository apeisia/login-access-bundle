<?php

namespace Apeisia\LoginAccess\Login;

use Apeisia\LoginAccess\Entity\AbstractAccount;
use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Entity\LoginAccess;
use Apeisia\LoginAccess\Service\AccountSelectionService;
use Doctrine\ORM\EntityManagerInterface;

class LoginAccount
{
    const ACCOUNT_USE_ANY = 'use-any';

    private ?AbstractAccount $account = null;
    private ?LoginAccess $loginAccess = null;
    private array $switchAccountHierarchy = [];
    private ?LoginAccess $originalAccess = null;

    /**
     * LoginAccount constructor.
     * @param EntityManagerInterface $em
     * @param AbstractLogin $login
     * @param $accountId
     * @param $originalAccountId
     * @param string $accountClass
     * @param string $loginAccessClass
     * @throws AccountSelectionRequiredException
     */
    public function __construct(
        private readonly EntityManagerInterface  $em,
        private readonly AccountSelectionService $accountSelectionService,
        AbstractLogin                            $login,
                                                 $accountId,
                                                 $originalAccountId,
        private readonly string                  $accountClass,
        private readonly string                  $loginAccessClass
    )
    {
        $this->account = $this->findAccount($login, $accountId, $originalAccountId);
    }

    /**
     * @param $accountId
     * @param AbstractLogin $login
     * @return LoginAccess|null
     */
    private function getAccess($accountId, AbstractLogin $login): ?LoginAccess
    {
        /** @var LoginAccess $loginAccess */
        $loginAccess = $this->em->getRepository($this->loginAccessClass)->findOneBy([
            'login'   => $login,
            'account' => $accountId,
        ]);

        return $loginAccess;
    }

    /**
     * @param AbstractLogin $login
     * @param $accountId
     * @param null $originalAccountId
     * @return AbstractAccount|mixed
     * @throws AccountSelectionRequiredException
     */
    private function findAccount(AbstractLogin $login, $accountId, $originalAccountId = null): mixed
    {
        if ($accountId && $accountId != self::ACCOUNT_USE_ANY) {
            // we know the account that we need. good.

            if ($originalAccountId) {
                // we are switching from one account to another.

                $originalAccess = $this->getAccess($originalAccountId, $login);
                if ($originalAccess) {
                    /** @var AbstractAccount $account */
                    $account = $this->em->getRepository($this->accountClass)->find($accountId);

                    if (!$this->switchedAccount($originalAccess, $account)) {
                        throw new AccountAccessDeniedException('Account switching denied');
                    }
                    $this->originalAccess = $originalAccess;
                } else {
                    throw new AccountAccessDeniedException('No access to original account');
                }
            } else {

                $access = $this->getAccess($accountId, $login);
                if ($access) {
                    $this->loginAccess = $access;
                    $account           = $access->getAccount();
                } else {
                    throw new AccountAccessDeniedException('No access to account');
                }
            }


        } else {
            $accounts = $this->accountSelectionService->getAvailableAccountsForLogin($login);
            if (count($accounts) == 0) {
                throw new AccountAccessDeniedException('No enabled account for login found');
            }

            if (count($accounts) == 1 || $accountId == self::ACCOUNT_USE_ANY) {
                $account = $accounts[0];
                // found one account. save the LoginAccess for later use
                $this->loginAccess = $this->em->getRepository($this->loginAccessClass)->findOneBy([
                    'login'   => $login,
                    'account' => $account,
                ]);
            } else {
                throw new AccountSelectionRequiredException($accounts);
            }
        }

        if ($account->isDisabled()) {
            throw new AccountAccessDeniedException('Account is disabled');
        }

        return $account;
    }


    private function switchedAccount(LoginAccess $originalAccess, AbstractAccount $targetAccount): bool
    {
        if (!method_exists($targetAccount, 'parents')) {
            return false;
        }
        $parentAccounts = [];
        $found          = false;
        foreach ($targetAccount->parents() as $parent) {
            $parentAccounts[] = $parent;
            if ($originalAccess->getAccount()->is($parent)) {
                $found = true;
                break;
            }
        }
        // is the target account a child of the original account?
        if (!$found)
            return false;


        // fake a access to $account
        $this->loginAccess = new $this->loginAccessClass($originalAccess->getLogin(), $targetAccount);
        $this->loginAccess->addRole('ROLE_SWITCHED_ACCOUNT');


        $this->switchAccountHierarchy = $parentAccounts;


        return true;
    }

    public function getAccount(): AbstractAccount
    {
        return $this->account;
    }

    public function getLoginAccess(): LoginAccess
    {
        return $this->loginAccess;
    }

    public function getSwitchAccountHierarchy(): array
    {
        return $this->switchAccountHierarchy;
    }

    public function getOriginalAccess(): ?LoginAccess
    {
        return $this->originalAccess;
    }
}
