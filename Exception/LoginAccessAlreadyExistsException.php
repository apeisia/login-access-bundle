<?php

namespace Apeisia\LoginAccess\Exception;

use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Entity\AbstractAccount;

class LoginAccessAlreadyExistsException extends \RuntimeException
{
    public static function create(AbstractAccount $account, AbstractLogin $login)
    {
        return new self(sprintf('A LoginAccess for the combination of account %d and login %d ("%s") already exists.',
            $account->getId(), $login->getId(), $login->getEmail()));
    }
}
