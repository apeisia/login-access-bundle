<?php
namespace Apeisia\LoginAccess\Security;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SwitchedAccountAllowEverythingVoter extends Voter
{
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        if(count($attributes) != 1 ||  substr($attributes[0], 0, 8) != 'ROLE_IS_') {
            foreach ($token->getRoleNames() as $role) {
                if ($role == 'ROLE_SWITCHED_ACCOUNT') {
                    return Voter::ACCESS_GRANTED;
                }
            }
        }

        return Voter::ACCESS_ABSTAIN;
    }

    protected function supports($attribute, $subject): bool
    {
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        return false;
    }


}
