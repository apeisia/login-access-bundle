<?php

namespace Apeisia\LoginAccess\Service;

use Apeisia\LoginAccess\Entity\AbstractAccount;
use AppBundle\Entity\Login;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AccountStorage
{
    private TokenStorageInterface $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function getCurrentAccount(?string $ensureAccountClass = null): ?AbstractAccount
    {
        $token = $this->tokenStorage->getToken();

        if (!$token) {
            return null;
        }

        $login = $token->getUser();

        if (!($login instanceof Login)) {
            return null;
        }

        $account = $login->getCurrentAccount();
        if($ensureAccountClass && !($account instanceof $ensureAccountClass)) {
            throw new \Exception('Expected $account of type ' . $ensureAccountClass . ' but got ' . get_class($account) . '.');
        }
        return $account;
    }
}
