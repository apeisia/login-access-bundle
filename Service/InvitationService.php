<?php

namespace Apeisia\LoginAccess\Service;

use Apeisia\LoginAccess\Entity\AbstractAccount;
use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Entity\Invitation;
use Apeisia\LoginAccess\Entity\LoginAccess;
use Apeisia\LoginAccess\Event\InvitationAcceptedEvent;
use Apeisia\LoginAccess\Event\InvitationCanceledEvent;
use Apeisia\LoginAccess\Event\InvitationInviteEvent;
use Apeisia\LoginAccess\Exception\LoginAccessAlreadyExistsException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class InvitationService
{
    private EntityManagerInterface $em;
    private ObjectRepository $invitationsRepository;
    private ObjectRepository $loginRepository;
    private ObjectRepository $accessRepository;
    private ?InvitationMailerInterface $invitationMailer;
    private string $loginAccessClass;
    private string $invitationClass;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        EntityManagerInterface     $em,
        RelationConfiguration      $relationConfiguration,
        ?InvitationMailerInterface $invitationMailer,
        EventDispatcherInterface   $eventDispatcher
    ) {
        $this->em                    = $em;
        $this->invitationsRepository = $em->getRepository($relationConfiguration->invitationClass);
        $this->loginRepository       = $em->getRepository($relationConfiguration->loginClass);
        $this->accessRepository      = $em->getRepository($relationConfiguration->loginAccessClass);
        $this->loginAccessClass      = $relationConfiguration->loginAccessClass;
        $this->invitationClass       = $relationConfiguration->invitationClass;
        $this->invitationMailer      = $invitationMailer;
        $this->eventDispatcher       = $eventDispatcher;
    }

    /**
     * Create or update an invitation for the given account and email address and send an email to the user.
     *
     * @param AbstractAccount $account The account for which the user is invited
     * @param string $email The email address the invitation should be send to
     * @param array $initialRoles
     * @param null|AbstractLogin $sentBy
     *
     * @return Invitation
     */
    public function invite(
        AbstractAccount $account,
        string          $email,
        array           $initialRoles,
        ?AbstractLogin  $sentBy
    ): Invitation {
        // check email address sanity
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException(sprintf('Valid email address expected, got "%s" instead.', $email));
        }

        // check if we already have an invitation for this combination
        /** @var Invitation $invitation */
        $invitation = $this->invitationsRepository->findOneBy([
            'account' => $account,
            'email'   => $email,
        ]);

        if ($invitation) {
            // existing invitation for this combination, update
            $invitation->setCreatedAt();
        } else {
            // create a new invitation
            $invitation = new $this->invitationClass($account, $email, $initialRoles, $sentBy);
        }

        // save invitation
        $this->em->persist($invitation);
        $this->eventDispatcher->dispatch(new InvitationInviteEvent($invitation));
        $this->em->flush();

        // send mail for confirmation
        if ($this->invitationMailer) {
            $this->invitationMailer->sendInvitationMail($invitation);
        }

        return $invitation;
    }

    /**
     * Delete an invitation.
     *
     * @param Invitation $invitation
     *
     * @return bool
     */
    public function cancel(Invitation $invitation)
    {
        $this->eventDispatcher->dispatch(new InvitationCanceledEvent($invitation));
        $this->em->remove($invitation);
        $this->em->flush();

        return true;
    }

    /**
     * Accept an invitation, deleting it from the database and creating an LoginAccess for the account and login.
     *
     * @param Invitation $invitation
     * @param AbstractLogin $login
     *
     * @return LoginAccess
     */
    public function accept(Invitation $invitation, AbstractLogin $login): LoginAccess
    {
        $account = $invitation->getAccount();

        if ($this->accessRepository->findOneBy([
                'account' => $account,
                'login'   => $login,
            ]) !== null) {
            throw LoginAccessAlreadyExistsException::create($account, $login);
        }

        /** @var LoginAccess $loginAccess */
        $loginAccess = new $this->loginAccessClass($login, $account);
        $loginAccess->setRoles($invitation->getRoles());
        $this->eventDispatcher->dispatch(new InvitationAcceptedEvent($invitation, $login));

        $this->em->persist($loginAccess);
        $this->em->remove($invitation);
        $this->em->flush();

        return $loginAccess;
    }
}
