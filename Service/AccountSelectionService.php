<?php

namespace Apeisia\LoginAccess\Service;

use Apeisia\LoginAccess\Entity\AbstractAccount;
use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Login\AccountAccessDeniedException;
use Doctrine\ORM\EntityManagerInterface;

class AccountSelectionService
{
    public function __construct(private readonly EntityManagerInterface $em,
                                private readonly string                 $accountClass)
    {
    }

    /**
     * @param AbstractLogin $login
     * @return AbstractAccount[]
     */
    public function getAvailableAccountsForLogin(AbstractLogin $login): array
    {
        $builder = $this->em->getRepository($this->accountClass)->createQueryBuilder('a');
        /** @var AbstractAccount[] $accounts */
        $accounts = $builder
            ->join('a.accesses', 'access')
            ->join('access.account', 'account')
            ->where('access.login=:login')
            ->andWhere('account.disabled=0')
            ->setParameter('login', $login)
            ->getQuery()
            ->execute();
        if (count($accounts) == 0) {
            throw new AccountAccessDeniedException('No account found for login');
        }

        // filter for the accounts with the maximum weight
        usort($accounts, fn(AbstractAccount $a,
                            AbstractAccount $b) => $b->getAccountWeight() <=> $a->getAccountWeight());
        $maxWeight = $accounts[0]->getAccountWeight();

        return array_filter($accounts, fn(AbstractAccount $a) => $a->getAccountWeight() == $maxWeight);
    }
}
