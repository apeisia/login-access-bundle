<?php

namespace Apeisia\LoginAccess\Service;

use Apeisia\BaseBundle\Generators\Random;
use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Event\ResetPasswordRequestEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;

class ResetPasswordService
{
    public function __construct(private readonly EntityManagerInterface      $entityManager,
                                private readonly RelationConfiguration       $relationConfiguration,
                                private readonly string                      $tokenTtl,
                                private readonly EventDispatcherInterface    $eventDispatcher,
                                private readonly UserPasswordHasherInterface $userPasswordHasher,
    )
    {
    }

    /**
     * If given login doesn't have a token or if it is expired, generate a new one (and set it to the user).
     * Returns the new token or null if no new token was generated.
     */
    public function generateTokenForLogin(AbstractLogin $login): ?string
    {
        if (!$this->isPasswordRequestExpired($login)) {
            // if user already has a valid token, don't generate a new one
            return null;
        }

        $confirmationToken = $this->generateTokenString();
        $login->setConfirmationToken($confirmationToken);

        return $confirmationToken;
    }

    /**
     * Finds a matching login for the given email address in the configured login database.
     */
    public function findLoginByEmail(string $email): ?AbstractLogin
    {
        $loginClass = $this->relationConfiguration->loginClass;
        /** @var AbstractLogin $login */
        $login = $this->entityManager->getRepository($loginClass)->findOneBy(['email' => $email]);

        return $login;
    }

    /**
     * Requests a password reset for the given email address, dispatching a ResetPasswordRequestEvent.
     * To use this, a handler for the ResetPasswordRequestEvent must be registered in the event dispatcher.
     *
     * @throws UserNotFoundException if no user is found for the given email address
     * @throws \RuntimeException if no handler processes the event
     * @throws \Exception if the event is not processed
     */
    public function requestPasswordReset(string $email): void
    {
        $login = $this->findLoginByEmail($email);
        if (null === $login) {
            throw new UserNotFoundException('No user found for email ' . $email);
        }

        $this->generateTokenForLogin($login);

        $event = new ResetPasswordRequestEvent($login);
        $this->eventDispatcher->dispatch($event);

        if (!$event->isProcessed()) {
            throw new \RuntimeException('No handler processed the password request event.');
        }

        $login->setPasswordRequestedAt(new \DateTime());
        $this->entityManager->flush();
    }

    /**
     * Checks if the password reset request for the given login is expired. If there was no request yet,
     * it's always expired.
     */
    public function isPasswordRequestExpired(AbstractLogin $login, ?\DateTimeImmutable $now = null): bool
    {
        if ($now === null) {
            $now = new \DateTimeImmutable();
        }

        if (!$login->getPasswordRequestedAt()) {
            // if there was no token requested yet, it's always handled as if it was expired
            return true;
        }

        $expirationInterval = new \DateInterval($this->tokenTtl);
        $expiresAt          = $login->getPasswordRequestedAt()->add($expirationInterval);

        return $expiresAt < $now;
    }

    /**
     * Finds a login by the given token. If the token is expired, null is returned.
     */
    public function findLoginByToken(string $token): ?AbstractLogin
    {
        $loginClass = $this->relationConfiguration->loginClass;
        /** @var AbstractLogin $login */
        $login = $this->entityManager->getRepository($loginClass)->findOneBy(['confirmationToken' => $token]);

        if (!$login || $this->isPasswordRequestExpired($login)) {
            return null;
        }

        return $login;
    }

    /**
     * Set the new password for the user, removing the token and the request date and flushing the entity manager.
     */
    public function setNewPassword(AbstractLogin $login, string $newPassword): void
    {
        $login->setPassword($this->userPasswordHasher->hashPassword($login, $newPassword));
        $login->setConfirmationToken(null);
        $login->setPasswordRequestedAt(null);
        $this->entityManager->flush();
    }

    public function generateTokenString(): string
    {
        return Random::generate('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-=_,', 32);
    }
}
