<?php

namespace Apeisia\LoginAccess\Service;

class RelationConfiguration
{

    public function __construct(
        public readonly string $loginClass,
        public readonly string $accountClass,
        public readonly string $loginAccessClass,
        public readonly string $invitationClass,
        public readonly string $enableInvitations,
    ) {
    }
}
