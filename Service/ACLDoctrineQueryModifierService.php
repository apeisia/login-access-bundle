<?php

namespace Apeisia\LoginAccess\Service;

use Apeisia\LoginAccess\Entity\ACLEntry;
use Apeisia\LoginAccess\Entity\LoginAccess;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

class ACLDoctrineQueryModifierService
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function addACLRestrictionToQuery(
        QueryBuilder $queryBuilder,
        string       $targetClass,
        string       $role,
        LoginAccess  $loginAccess
    ) {
        if(!$this->em->contains($loginAccess)) {
            return null; // switched account. can not have any acl -> no restriction
        }
        $ids     = [];
        $entries = $this->em->getRepository(ACLEntry::class)
            ->createQueryBuilder('a')
            ->where('a.loginAccess = :loginAccess')
            ->andWhere('a.targetClass = :targetClass')
            ->andWhere('(a.validFrom <= :now OR a.validFrom IS NULL)')
            ->andWhere('(a.validUntil >= :now OR a.validUntil IS NULL)')
            ->setParameter('targetClass', $targetClass)
            ->setParameter('loginAccess', $loginAccess)
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->execute();
        /** @var ACLEntry $entry */
        foreach ($entries as $entry) {
            if (!in_array($role, $entry->getRoles())) {
                continue;
            }
            if ($entry->getId() === null) {
                return; // access to complete list
            }
            $ids[] = $entry->getTargetId();
        }
        if (count($ids) > 0) {
            // restrict access to specific ids
            $queryBuilder
                ->andWhere('a.id IN(:ids)')
                ->setParameter('ids', $ids);
        }
    }
}
