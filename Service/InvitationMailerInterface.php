<?php

namespace Apeisia\LoginAccess\Service;

use Apeisia\LoginAccess\Entity\Invitation;

/**
 * @deprecated Use InvitationInviteEvent instead
 */
interface InvitationMailerInterface
{
    /**
     * @deprecated Use InvitationInviteEvent instead
     */
    public function sendInvitationMail(Invitation $invitation): void;
}
