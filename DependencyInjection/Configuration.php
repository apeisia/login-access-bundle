<?php

namespace Apeisia\LoginAccess\DependencyInjection;

use Apeisia\LoginAccess\Entity\Invitation;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): \Symfony\Component\Config\Definition\Builder\TreeBuilder
    {
        $treeBuilder = new TreeBuilder('apeisia_login_access');
        $rootNode    = $treeBuilder->getRootNode();

        // https://stackoverflow.com/questions/3375307/how-to-disable-code-formatting-for-some-part-of-the-code-using-comments
        // @formatter:off
        $rootNode
            ->children()
                ->booleanNode('enable_invitations')->defaultTrue()->end()
                ->scalarNode('login_class')->isRequired()->end()
                ->scalarNode('account_class')->isRequired()->end()
                ->scalarNode('loginaccess_class')->isRequired()->end()
                ->scalarNode('invitation_class')->defaultValue(Invitation::class)->end()
                ->scalarNode('invitation_mailer')->defaultNull()->end()
                ->scalarNode('switched_roles_strategy')->defaultValue('allow_everything')->end()
                ->scalarNode('password_reset_token_ttl')->defaultValue('P1D')->end()
            ->end()
        ;
        // @formatter:on

        return $treeBuilder;
    }
}
