<?php

namespace Apeisia\LoginAccess\DependencyInjection;

use Apeisia\LoginAccess\Controller\SecurityController;
use Apeisia\LoginAccess\Firewall\AccountListener;
use Apeisia\LoginAccess\Login\AccountManager;
use Apeisia\LoginAccess\Security\SwitchedAccountAllowEverythingVoter;
use Apeisia\LoginAccess\Service\AccountSelectionService;
use Apeisia\LoginAccess\Service\InvitationService;
use Apeisia\LoginAccess\Service\RelationConfiguration;
use Apeisia\LoginAccess\Service\ResetPasswordService;
use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ApeisiaLoginAccessExtension extends Extension
{
    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $container
            ->register(RelationConfiguration::class)
            ->setArguments([
                $config['login_class'],
                $config['account_class'],
                $config['loginaccess_class'],
                $config['invitation_class'],
                $config['enable_invitations'],
            ])
        ;

        $container->getDefinition(AccountManager::class)
            ->setArgument('$accountClass', $config['account_class'])
            ->setArgument('$loginAccessClass', $config['loginaccess_class'])
        ;
        $container->getDefinition(AccountListener::class)
            ->setArgument('$loginClass', $config['login_class'])
        ;

        $container->getDefinition(SecurityController::class)
            ->setArgument('$loginClass', $config['login_class'])
        ;

        $container->getDefinition(AccountSelectionService::class)
            ->setArgument('$accountClass', $config['account_class'])
        ;

        $container->getDefinition(ResetPasswordService::class)
            ->setArgument('$tokenTtl', $config['password_reset_token_ttl'])
        ;

        if ($config['invitation_mailer']) {
            $container->getDefinition(InvitationService::class)
                ->setArgument('$invitationMailer', new Reference($config['invitation_mailer']))
            ;
        }

        switch ($config['switched_roles_strategy']) {
            case 'allow_everything':
                // switched accounts get all roles (every check is granted)
                $container->register(SwitchedAccountAllowEverythingVoter::class)
                    ->addTag('security.voter')
                ;
                break;

            case 'account':
                // switched accounts get roles that are returned by accounts getAdditionalRolesForSwitchedAccount()
                $container->getDefinition(AccountListener::class)
                    ->addMethodCall('enableAccountSwitchedRolesCheck')
                ;
                break;

            case 'none':
                // don't set ANY roles when account is switched (aka handle it in project)
                break;

            default:
                throw new InvalidArgumentException('Unknown switched_role_strategy given.');
        }
    }
}
