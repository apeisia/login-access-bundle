<?php

namespace Apeisia\LoginAccess\Firewall;

use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Login\AccountManager;
use Apeisia\LoginAccess\Login\LoginAccount;
use Apeisia\LoginAccess\Repository\AbstractLoginRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class AccountListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return array(
            KernelEvents::REQUEST => [
                'onRequest',
                2000,
            ],
        );
    }

    protected TokenStorageInterface $tokenStorage;
    protected AccountManager $accountManager;
    protected RoleHierarchyInterface $roleHierarchy;
    private bool $doAccountSwitchedRolesCheck = false;

    public function __construct(
        TokenStorageInterface          $tokenStorage,
        AccountManager                 $accountManager,
        RoleHierarchyInterface         $roleHierarchy,
        private readonly Security      $security,
        private EntityManagerInterface $em,
        private string                 $loginClass
    )
    {
        $this->tokenStorage   = $tokenStorage;
        $this->accountManager = $accountManager;
        $this->roleHierarchy  = $roleHierarchy;
    }

    public function onRequest(RequestEvent $event): void
    {
        //return;
        if (!$event->isMainRequest())
            return;
        $repository = $this->em->getRepository($this->loginClass);
        if (!$repository instanceof AbstractLoginRepository) {
            throw new Exception('Repository of $loginClass class must be instance of AbstractLoginRepository');
        }

//        $login = $this->security->getUser();
//        if (!$login) return;

        $repository->setOnLoginLoad(function (AbstractLogin $login) use ($event) {
            $this->onLoginLoad($event->getRequest(), $login);
        });
    }

    public function onLoginLoad(Request $request, AbstractLogin $login)
    {
        $accountId         = null;
        $originalAccountId = null;

        $head = $request->headers->get('x-account');
        if ($head) {
            $accountId = $head;
        }
        $param = $request->get('_as_account');
        if ($param) {
            $accountId = $param;
        }

        if ($accountId !== null && str_contains($accountId, ':')) {
            [$originalAccountId, $accountId] = explode(':', $accountId, 2);
        }
        if ($accountId !== null && str_contains($accountId, ';')) {
            [$originalAccountId, $accountId] = explode(';', $accountId, 2);
        }
        if ($originalAccountId == 'null')
            $originalAccountId = null;
        if ($accountId == 'null')
            $accountId = null;


        if ($request->getPathInfo() === '/api/login'
            || $request->getPathInfo() === '/api/account-selection') {
            $accountId = LoginAccount::ACCOUNT_USE_ANY;
        }

        $loginAccess = $this->accountManager->getLoginAccess(
            $this->accountManager->getAccount($login, $accountId, $originalAccountId)
        );

        if (in_array('ROLE_SWITCHED_ACCOUNT', $loginAccess->getRoles()) && $this->doAccountSwitchedRolesCheck) {
            foreach ($loginAccess->getAccount()->getAdditionalRolesForSwitchedAccount() as $additionalRole) {
                $loginAccess->addRole($additionalRole);
            }
        }

        $login->setCurrentAccess($loginAccess);
    }

    public function enableAccountSwitchedRolesCheck(): void
    {
        $this->doAccountSwitchedRolesCheck = true;
    }
}
