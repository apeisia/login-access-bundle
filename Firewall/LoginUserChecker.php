<?php

namespace Apeisia\LoginAccess\Firewall;

use Apeisia\LoginAccess\Entity\AbstractLogin;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class LoginUserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof AbstractLogin) {
            return;
        }

        if (!$user->isEnabled()) {
            $ex = new CustomUserMessageAccountStatusException('User account is disabled.');
            $ex->setUser($user);
            throw $ex;
        }
    }

    public function checkPostAuth(UserInterface $user): void
    {
    }
}
