<?php

namespace Apeisia\LoginAccess\RoleMap;

use Symfony\Component\Yaml\Yaml;

/**
 * Generate the security.yaml file that is required for idea.
 */
class RoleMapSecurityYamlGenerator
{
    /**
     * @var RoleMapService
     */
    private RoleMapService $roleMapService;

    private string $targetPath;

    public function __construct(RoleMapService $roleMapService, string $projectDir)
    {
        $this->roleMapService = $roleMapService;
        $this->targetPath     = $projectDir . '/var/security.yaml';
    }

    public function generate()
    {
        $roleNames = array_keys($this->roleMapService->getRoles());
        $comment   = '# File is auto generated from /config/roles.yaml, see LoginAccessBundle RoleMapSecurityYamlGenerator.' . PHP_EOL . PHP_EOL;
        file_put_contents($this->targetPath, $comment . Yaml::dump([
                'security' => [
                    'role_hierarchy' => [
                        'DUMMY' => $roleNames,
                    ],
                ],
            ], 4));
    }
}
