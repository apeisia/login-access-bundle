<?php

namespace Apeisia\LoginAccess\RoleMap;

use Apeisia\WatchBundle\Event\FileChangedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

if (class_exists('Apeisia\\WatchBundle\\Event\\FileChangedEvent')) {
    class FileChangedSubscriber implements EventSubscriberInterface
    {
        private RoleMapService $roleMapService;
        private RoleMapJsonGenerator $jsonGenerator;
        private RoleMapSecurityYamlGenerator $securityYamlGenerator;

        public function __construct(RoleMapService $roleMapService,
                                    RoleMapJsonGenerator $jsonGenerator,
                                    RoleMapSecurityYamlGenerator $securityYamlGenerator)
        {
            $this->roleMapService        = $roleMapService;
            $this->jsonGenerator         = $jsonGenerator;
            $this->securityYamlGenerator = $securityYamlGenerator;
        }

        public static function getSubscribedEvents(): array
        {
            return [
                FileChangedEvent::class => 'onFileChanged',
            ];
        }

        public function onFileChanged(FileChangedEvent $event)
        {
            if ($event->getPath() !== $this->roleMapService->getRoleMapPath()) {
                return;
            }

            $this->roleMapService->load();
            $this->jsonGenerator->generate();
            $this->securityYamlGenerator->generate();
        }
    }
} else {
    class FileChangedSubscriber implements EventSubscriberInterface
    {
        //
        // empty fallback if no watch bundle is installed
        //

        public function __construct(RoleMapService $roleMapService,
                                    RoleMapJsonGenerator $javascriptGenerator,
                                    RoleMapSecurityYamlGenerator $securityYamlGenerator)
        {

        }

        public static function getSubscribedEvents(): array
        {
            return [];
        }
    }
}
