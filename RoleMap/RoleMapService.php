<?php

namespace Apeisia\LoginAccess\RoleMap;

use Symfony\Component\Yaml\Yaml;

/**
 * Read the role map as defined in /config/roles.yaml
 */
class RoleMapService
{
    private string $roleMapPath;

    /**
     * All defined roles, including their configuration (name, icon)
     * @var array
     */
    private array $roles = [];

    /**
     * Available roles by account type.
     * @var array
     */
    private array $availableRoles = [];

    public function __construct(string $projectDir)
    {
        $this->roleMapPath = $projectDir . '/config/roles.yaml';
        $this->load();
    }

    public function getRoleMapPath()
    {
        return $this->roleMapPath;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getAvailableRolesByAccountType()
    {
        return $this->availableRoles;
    }

    public function accountTypeSupportsAttribute($accountType, $attribute)
    {
        return in_array($attribute, $this->availableRoles[$accountType] ?? []);
    }

    public function load()
    {
        if (!file_exists($this->roleMapPath)) {
            return;
        }

        $input                = Yaml::parseFile($this->roleMapPath);
        $this->roles          = $input['roles'];
        $this->availableRoles = $input['availableRoles'];

        // normalize roles
        foreach ($this->roles as $key => $value) {
            if ($value === null) {
                // ROLE_NAME: ~
                $value = ['name' => null, 'icon' => null];
            }

            $value['name'] ??= 'security.role.' . $key;
            $value['icon'] ??= null;

            $this->roles[$key] = $value;
        }
    }
}
