<?php

namespace Apeisia\LoginAccess\RoleMap;

/**
 * Generate the roles.ts file that is required by the javascript frontend.
 */
class RoleMapJsonGenerator
{
    /**
     * @var RoleMapService
     */
    private RoleMapService $roleMapService;

    private string $targetPath;

    public function __construct(RoleMapService $roleMapService, string $projectDir)
    {
        $this->roleMapService = $roleMapService;
        $this->targetPath     = $projectDir . '/au_out/security_roles.json';
    }

    public function generate()
    {
//        file_put_contents($this->targetPath, json_encode([
//            'roles'          => $this->roleMapService->getRoles(),
//            'availableRoles' => $this->roleMapService->getAvailableRolesByAccountType(),
//        ], JSON_PRETTY_PRINT));
    }
}
