<?php

namespace Apeisia\LoginAccess\Repository;

use Apeisia\LoginAccess\Entity\AbstractLogin;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class AbstractLoginRepository extends EntityRepository implements UserProviderInterface, UserLoaderInterface
{
    /**
     * @var callable
     */
    private $onLoginLoad;

    public function setOnLoginLoad(?callable $onLoginLoad)
    {
        $this->onLoginLoad = $onLoginLoad;
    }

    private function callOnLoginLoad(AbstractLogin $login): AbstractLogin
    {
        $fn = $this->onLoginLoad;
        if ($fn) {
            $fn($login);
        }
        return $login;
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        return $this->callOnLoginLoad($this->find($user->getId()));
    }

    public function supportsClass(string $class): bool
    {
        return $class == AbstractLogin::class;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $login = $this->createQueryBuilder('a')
            ->where('a.username = :identifier OR a.email = :identifier')
            ->setParameter('identifier', $identifier)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        if (!$login) {
            $e = new UserNotFoundException(sprintf('Login "%s" not found.', $identifier));
            $e->setUserIdentifier($identifier);

            throw $e;
        }
        return $this->callOnLoginLoad($login);
    }
}
