<?php

namespace Apeisia\LoginAccess\Controller;

use Apeisia\BaseBundle\Service\ObjectToArrayTransformer;
use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Event\ResetPasswordRequestEvent;
use Apeisia\LoginAccess\Service\AccountSelectionService;
use DateTime;
use Apeisia\LoginAccess\Service\ResetPasswordService;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Psr\EventDispatcher\EventDispatcherInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class SecurityController extends AbstractController
{

    public function __construct(
        private readonly TokenStorageInterface $tokenStorage,
        private readonly string                $loginClass,
    )
    {
    }

    #[Rest\Post("/login", name: "security_login")]
    public function loginAction(Request $request): void
    {
        // Don't let this empty controller confuse you.
        // When you submit a POST request to the /login URL with the following JSON document as the body,
        // the security system intercepts the requests. It takes care of authenticating the user with the
        // submitted username and password or triggers an error in case the authentication process fails
    }

    #[Rest\Get("/account-selection", name: "security_account_selection")]
    public function accountSelectionAction(AccountSelectionService  $accountSelectionService,
                                           Security                 $security,
                                           ObjectToArrayTransformer $objectToArrayTransformer): array
    {
        /** @var AbstractLogin $login */
        $login             = $security->getUser();
        $availableAccounts = $accountSelectionService->getAvailableAccountsForLogin($login);

        return $objectToArrayTransformer->toArray($availableAccounts, ['select']);
    }

    #[Rest\Post("/logout", name: "security_logout")]
    public function logoutAction(Request $request): void
    {
        $this->tokenStorage->setToken(null);
        $request->getSession()->invalidate();
    }

    #[Rest\Put("/change_password")]
    public function changePasswordAction(Request $request, UserPasswordHasherInterface $passwordHasher): View
    {
        /** @var AbstractLogin $user */
        $user = $this->getUser();

        $new = $request->get('new_password');
        if ($new != $request->get('confirm_password')) {
            return View::create('mismatch', 400);
        }

        if (!$passwordHasher->isPasswordValid($user, $request->get('old_password'))) {
            return View::create('invalid', 400);
        }

        $user->setPlainPassword($new);

        return View::create(null, 204);
    }


    #[Rest\Post("/request_password")]
    public function resetPasswordRequestAction(Request              $request,
                                               ResetPasswordService $resetPasswordService
    ): View
    {
        $resetPasswordService->requestPasswordReset($request->get('email'));

        return View::create(null, 204);
    }


    /**
     * @Rest\Post("/reset_password")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return View
     */
    public function resetAction(
        Request                $request,
        EntityManagerInterface $em,
        ResetPasswordService   $resetPasswordService
    ): View
    {
        $token  = $request->get('token');
        $first  = $request->get('newPassword');
        $second = $request->get('newPasswordRepeat');

        $login = $resetPasswordService->findLoginByToken($token);
        if (null === $login) {
            throw $this->createNotFoundException();
        }

        $expired = !$login->isPasswordRequestNonExpired(24 * 3600);

        if ($expired) {
            throw new BadRequestHttpException('Token expired');
        }

        if ($first == $second) {
            $resetPasswordService->setNewPassword($login, $first);
            $em->flush();

            return View::create(null, 204);
        }

        throw new BadRequestHttpException();
    }


}
