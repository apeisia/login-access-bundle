<?php

namespace Apeisia\LoginAccess\SecurityVoters;

use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Entity\ACLEntry;
use Apeisia\LoginAccess\Login\AccountManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class AclVoter implements VoterInterface
{
    private EntityManagerInterface $em;
    private AccountManager $accountManager;
    private RoleHierarchyInterface $roleHierarchy;

    public function __construct(EntityManagerInterface $em, AccountManager $accountManager, RoleHierarchyInterface $roleHierarchy)
    {
        $this->em = $em;
        $this->accountManager = $accountManager;
        $this->roleHierarchy = $roleHierarchy;
    }

    /**
     * @inheritDoc
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        $login = $token->getUser();
        if (!($login instanceof AbstractLogin) || !$login->getCurrentAccount() || $this->accountManager->isSwitchedAccount($login->getCurrentAccount())) {
            return self::ACCESS_ABSTAIN;
        }

        $targetId = null;
        if (is_object($subject)) {
            if (method_exists($subject, 'getId')) {
                $targetId = $subject->getId();
            }
            $subject = get_class($subject);
        }
        $qb = $this->em->getRepository(ACLEntry::class)
            ->createQueryBuilder('a')
            ->where('a.loginAccess = :loginAccess');

        if ($subject) {
            $qb
                ->andWhere('a.targetClass = :targetClass')
                ->setParameter('targetClass', $subject);
            if ($targetId) {
                $qb
                    ->andWhere('a.targetId IN (:targetId)')
                    ->setParameter('targetId', [null, $targetId]);
            }
        }

        $entries = $qb
            ->andWhere('(a.validFrom <= :now OR a.validFrom IS NULL)')
            ->andWhere('(a.validUntil >= :now OR a.validUntil IS NULL)')
            ->setParameter('loginAccess', $login->getCurrentAccess())
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->execute();
        foreach ($entries as $entry) {
            foreach ($attributes as $attribute) {
                $roles = $this->roleHierarchy->getReachableRoleNames($entry->getRoles());
                if (in_array($attribute, $roles)) {
                    return self::ACCESS_GRANTED;
                }
            }
        }

        return self::ACCESS_ABSTAIN;
    }
}
