<?php

namespace Apeisia\LoginAccess\SecurityVoters;

use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Login\AccountManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class ActualRolesVoter implements VoterInterface
{
    private EntityManagerInterface $em;
    private AccountManager $accountManager;
    private RoleHierarchyInterface $roleHierarchy;

    public function __construct(EntityManagerInterface $em, AccountManager $accountManager, RoleHierarchyInterface $roleHierarchy)
    {
        $this->em             = $em;
        $this->accountManager = $accountManager;
        $this->roleHierarchy  = $roleHierarchy;
    }

    /**
     * @inheritDoc
     */
    public function vote(TokenInterface $token, $subject, array $attributes): int
    {
        $login = $token->getUser();
        if (!($login instanceof AbstractLogin)) {
            return self::ACCESS_ABSTAIN;
        }

        $roles = $this->roleHierarchy->getReachableRoleNames($login->getCombinedRoles());

        foreach ($attributes as $attribute) {
            if (in_array($attribute, $roles)) {
                return self::ACCESS_GRANTED;
            }
        }

        return self::ACCESS_ABSTAIN;
    }
}
