<?php

namespace Apeisia\LoginAccess\Event;

use Apeisia\LoginAccess\Entity\AbstractLogin;
use Apeisia\LoginAccess\Entity\Invitation;

class InvitationAcceptedEvent extends InvitationEvent
{
    public function __construct(Invitation $invitation, private readonly AbstractLogin $login)
    {
        parent::__construct($invitation);
    }

    public function getLogin(): AbstractLogin
    {
        return $this->login;
    }
}
