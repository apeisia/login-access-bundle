<?php

namespace Apeisia\LoginAccess\Event;

use Apeisia\LoginAccess\Entity\AbstractLogin;
use Symfony\Contracts\EventDispatcher\Event;

class ResetPasswordRequestEvent extends Event
{
    private AbstractLogin $login;
    private bool $processed;

    /**
     * User requested a password reset
     *
     * @param AbstractLogin $login
     */
    public function __construct(AbstractLogin $login)
    {
        $this->login = $login;
    }

    public function getLogin(): AbstractLogin
    {
        return $this->login;
    }

    /**
     * This must be called at least once or an exception will get thrown.
     *
     * @return void
     */
    public function setProcessed(): void
    {
        $this->processed = true;
    }

    public function isProcessed(): bool
    {
        return $this->processed;
    }
}
