<?php

namespace Apeisia\LoginAccess\Event;

use Apeisia\LoginAccess\Entity\Invitation;
use Symfony\Contracts\EventDispatcher\Event;

class InvitationEvent extends Event
{
    public function __construct(private readonly Invitation $invitation)
    {
    }

    public function getInvitation(): Invitation
    {
        return $this->invitation;
    }

}
