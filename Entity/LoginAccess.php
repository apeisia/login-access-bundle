<?php

namespace Apeisia\LoginAccess\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[UniqueEntity(["login", "account"])]
abstract class LoginAccess
{
    use TimestampableEntity;

    // note that the actual relation is set in the DoctrineMetadataSubscriber
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    #[Groups(['LoginAccess.login'])]
    protected $login;

    // note that the actual relation is set in the DoctrineMetadataSubscriber
    #[ORM\JoinColumn(nullable: false, onDelete: 'CASCADE')]
    #[Groups(['LoginAccess.account'])]
    protected $account;

    /**
     * @var string[]
     */
    #[ORM\Column(type: "json")]
    #[Groups(['LoginAccess.roles'])]
    protected $roles;

    /**
     * @var Collection
     */
    protected $aclEntries;

    public function __construct(AbstractLogin $login, AbstractAccount $account)
    {
        $this->login   = $login;
        $this->account = $account;
        $this->roles   = [];
    }

    public function getLogin(): AbstractLogin
    {
        return $this->login;
    }

    public function getAccount(): AbstractAccount
    {
        return $this->account;
    }

    public function addRole(string $role): self
    {
        $role = strtoupper($role);
        if ($role === 'ROLE_USER') {
            return $this;
        }

        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function addRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = array_values($roles);

        return $this;
    }

    public function __toString()
    {
        return $this->login->__toString();
    }

    public function getACLEntries(): Collection
    {
        return $this->aclEntries;
    }

    public function addACLEntry(ACLEntry $aclEntry): self
    {
        if (!$this->aclEntries->contains($aclEntry)) {
            $this->aclEntries->add($aclEntry);
        }

        return $this;
    }

    public function removeACLEntry(ACLEntry $aclEntry): self
    {
        if ($this->aclEntries->contains($aclEntry)) {
            $this->aclEntries->removeElement($aclEntry);
        }

        return $this;
    }

}
