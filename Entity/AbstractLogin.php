<?php

namespace Apeisia\LoginAccess\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Exception;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[UniqueEntity('email')]
#[UniqueEntity('username')]
abstract class AbstractLogin implements UserInterface, EquatableInterface, PasswordAuthenticatedUserInterface
{
    use TimestampableEntity;

    #[ORM\Column(type: "string", length: 180, unique: true)]
    #[Serializer\Groups(["default", "list"])]
    protected string $username;

    #[ORM\Column(type: "string", length: 180, unique: true)]
    #[Serializer\Groups(["default", "list"])]
    protected string $email;

    #[ORM\Column(type: "json")]
    protected array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column(type: "string")]
    protected string $password;

    #[ORM\Column(length: 180, nullable: true)]
    protected ?string $confirmationToken = null;

    #[ORM\Column]
    #[Serializer\Groups(["Login.enabled"])]
    protected bool $enabled = true;

    #[ORM\Column(nullable: true)]
    protected ?\DateTime $lastLogin = null;

    #[ORM\Column(nullable: true)]
    protected ?\DateTime $passwordRequestedAt = null;

    protected ?string $plainPassword = null;

    /**
     * @var LoginAccess[]|PersistentCollection
     */
    #[Serializer\Groups(["Login.accesses"])]
    protected iterable $accesses;

    protected ?LoginAccess $currentLoginAccess = null;

    public function __construct()
    {
        $this->accesses = new ArrayCollection();
    }

    public function __sleep()
    {
        return ['id', 'username', 'email'];
    }

    /**
     * @return LoginAccess[]
     */
    public function getAccesses(): array
    {
        if (!$this->accesses)
            return [];

        return $this->accesses->toArray();
    }

    public function setCurrentAccess(LoginAccess $loginAccess)
    {
        $this->currentLoginAccess = $loginAccess;
    }

    public function getCurrentAccess(): ?LoginAccess
    {
        if (!$this->currentLoginAccess) {
            throw new Exception('The current account and access can only be retrieved from an Login object in the token storage');
        }

        return $this->currentLoginAccess;
    }

    public function getCurrentAccount(): ?AbstractAccount
    {
        return $this->getCurrentAccess()?->getAccount();
    }

    public function isEqualTo(UserInterface $user): bool
    {
        if (!method_exists($this, 'getId') || !method_exists($user, 'getId')) {
            throw new Exception('cannot compare two users without a getId() method');
        }
        if ($this->getId() !== $user->getId()) {
            return false;
        }

        if ($this->getUsername() !== $user->getUsername()) {
            return false;
        }

        if ($this->getEmail() !== $user->getEmail()) {
            return false;
        }


        return true;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    final public function getRoles(): array
    {
        // only save ROLE_USER in the session. see getActualRoles()
        return ['ROLE_USER'];
    }

    /**
     * @see UserInterface
     */
    public function getCombinedRoles(): array
    {
        $account = $this->currentLoginAccess->getAccount();
        $roles   = array_merge(
            $this->currentLoginAccess->getRoles(),
            $this->getLoginRoles(),
            $account->getRoles(),
        );
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_values(array_unique($roles));
    }

    public function getLoginRoles(): array
    {
        return $this->roles;
    }

    public function setLoginRoles(array $roles): self
    {
        $this->roles = array_values($roles);

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;
        $this->updatedAt     = new DateTime(); // required to trigger doctrine listener

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getPasswordRequestedAt(): ?DateTime
    {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt(?DateTime $passwordRequestedAt): self
    {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }


    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    abstract function getId();

    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }

    public function isPasswordRequestNonExpired(int $seconds): bool
    {
        return !$this->getPasswordRequestedAt() ||
            $this->getPasswordRequestedAt()->getTimestamp() + $seconds > time();
    }
}
