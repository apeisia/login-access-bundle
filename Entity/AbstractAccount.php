<?php

namespace Apeisia\LoginAccess\Entity;

use Apeisia\BaseBundle\Entity\CompareTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use JMS\Serializer\Annotation as Serializer;

abstract class AbstractAccount
{
    use CompareTrait;

    /**
     * @var LoginAccess[]|Collection<LoginAccess>
     */
    protected iterable $accesses;

    /**
     * @var Invitation[]|Collection<Invitation>
     */
    protected iterable $pendingInvitations;

    #[ORM\Column(type: "boolean")]
    #[Serializer\Groups(["Default", "default", "list"])]
    protected bool $disabled = false;

    abstract public function getId(): string|int|null;

    public function __construct()
    {
        $this->accesses           = new ArrayCollection();
        $this->pendingInvitations = new ArrayCollection();
    }

    /**
     * @return Invitation[]|Collection<Invitation>
     */
    public function getPendingInvitations(): iterable
    {
        return $this->pendingInvitations;
    }

    /**
     * @return LoginAccess[]|Collection<LoginAccess>
     */
    public function getAccesses(): iterable
    {
        return $this->accesses;
    }

    #[Pure]
    public function getRoles(): array
    {
        return [];
    }

    #[Pure]
    public function getAccountWeight(): int
    {
        return 10;
    }

    /**
     * If the config option "switched_role_strategy" is set to "account", this method will be called to get
     * the roles that should be added if the user is switched to this account.
     *
     * @return array
     */
    public function getSwitchedRoles(): iterable
    {
        return [];
    }

    #[Pure]
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

}
