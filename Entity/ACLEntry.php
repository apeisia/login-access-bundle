<?php

namespace Apeisia\LoginAccess\Entity;

use Apeisia\BaseBundle\Entity\EntityUUId;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity()
 * @ORM\Table(name="acl_entry")
 */
#[ORM\Entity]
#[ORM\Table(name: "acl_entry")]
class ACLEntry
{
    use EntityUUId;

    /**
     * @ORM\JoinColumn(nullable=false)
     */
    #[ORM\JoinColumn(nullable: false)]
    private ?LoginAccess $loginAccess = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Serializer\Groups({"AclEntry"})
     */
    #[ORM\Column(type: "string", nullable: false)]
    #[Serializer\Groups(["AclEntry"])]
    private ?string $targetClass = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Serializer\Groups({"AclEntry"})
     */
    #[ORM\Column(type: "string", nullable: false)]
    #[Serializer\Groups(["AclEntry"])]
    private ?string $targetId = null;

    /**
     * @ORM\Column(type="json", nullable=false)
     * @Serializer\Groups({"AclEntry"})
     */
    #[ORM\Column(type: "json", nullable: false)]
    #[Serializer\Groups(["AclEntry"])]
    private ?array $roles = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Serializer\Groups({"AclEntry"})
     */
    #[ORM\Column(type: "datetime_immutable", nullable: true)]
    #[Serializer\Groups(["AclEntry"])]
    private ?\DateTimeImmutable $validFrom = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     * @Serializer\Groups({"AclEntry"})
     */
    #[ORM\Column(type: "datetime_immutable", nullable: true)]
    #[Serializer\Groups(["AclEntry"])]
    private ?\DateTimeImmutable $validUntil = null;

    /**
     * @return LoginAccess|null
     */
    public function getLoginAccess(): ?LoginAccess
    {
        return $this->loginAccess;
    }

    /**
     * @param LoginAccess|null $loginAccess
     * @return self
     */
    public function setLoginAccess(?LoginAccess $loginAccess): self
    {
        $this->loginAccess = $loginAccess;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTargetClass(): ?string
    {
        return $this->targetClass;
    }

    /**
     * @param string|null $targetClass
     * @return self
     */
    public function setTargetClass(?string $targetClass): self
    {
        $this->targetClass = $targetClass;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTargetId(): ?string
    {
        return $this->targetId;
    }

    /**
     * @param string|null $targetId
     * @return self
     */
    public function setTargetId(?string $targetId): self
    {
        $this->targetId = $targetId;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getRoles(): ?array
    {
        return $this->roles;
    }

    /**
     * @param array|null $roles
     * @return self
     */
    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getValidFrom(): ?\DateTimeImmutable
    {
        return $this->validFrom;
    }

    /**
     * @param \DateTimeImmutable|null $validFrom
     * @return self
     */
    public function setValidFrom(?\DateTimeImmutable $validFrom): self
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getValidUntil(): ?\DateTimeImmutable
    {
        return $this->validUntil;
    }

    /**
     * @param \DateTimeImmutable|null $validUntil
     * @return self
     */
    public function setValidUntil(?\DateTimeImmutable $validUntil): self
    {
        $this->validUntil = $validUntil;

        return $this;
    }


}
