<?php

namespace Apeisia\LoginAccess\Entity;

use Doctrine\ORM\Mapping as ORM;

class Invitation
{

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=32, unique=true)
     */
    #[ORM\Column(name: "token", type: "string", length: 32, unique: true)]
    protected $token;

    /**
     * @var string
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    #[ORM\JoinColumn(nullable: false, onDelete: "CASCADE")]
    protected $account;

    /**
     * @var string[]
     * @ORM\Column(type="array")
     */
    #[ORM\Column(type: "array")]
    protected $roles;

    /**
     * @var AbstractLogin
     *
     * @ORM\ManyToOne(targetEntity="Login")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    #[ORM\ManyToOne(targetEntity: "Login")]
    #[ORM\JoinColumn(nullable: true, onDelete: "CASCADE")]
    protected $sentBy;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    #[ORM\Column(name: "email", type: "string", length: 255)]
    protected $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    #[ORM\Column(name: "created_at", type: "datetime", nullable: false)]
    protected $createdAt;

    public function __construct(AbstractAccount $account, string $email, array $roles, ?AbstractLogin $sentBy = null)
    {
        $this->createdAt = new \DateTime();
        $this->token     = bin2hex(random_bytes(16));
        $this->account   = $account;
        $this->email     = $email;
        $this->sentBy    = $sentBy;
        $this->roles     = $roles;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return Invitation
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set account
     *
     * @param AbstractAccount $account
     *
     * @return Invitation
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return AbstractAccount
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Invitation
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return self
     */
    public function setCreatedAt(?\DateTime $createdAt = null): self
    {
        if ($createdAt === null) {
            $createdAt = new \DateTime();
        }

        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return AbstractLogin
     */
    public function getSentBy(): ?AbstractLogin
    {
        return $this->sentBy;
    }

    /**
     * @param AbstractLogin $sentBy
     *
     * @return self
     */
    public function setSentBy(AbstractLogin $sentBy): self
    {
        $this->sentBy = $sentBy;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}

